export interface UserI{
    presupuesto: number;
    unidad: string;
    producto: string;
    cantidad: number;
    valorunitario: number;
    valortotal: number;
    fechadquisicion: Date;
    proveedor: string;
    id: string
    
}