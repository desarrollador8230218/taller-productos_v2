import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule, NavController, ToastController } from '@ionic/angular';
import { UserI } from '../common/models/users.models';
import { FirestoreService } from '../common/services/firestore.service';
import { Router } from '@angular/router';
import { collectionData, collection, Firestore } from '@angular/fire/firestore';
import { firstValueFrom } from 'rxjs';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.page.html',
  styleUrls: ['./create-product.page.scss'],
  standalone: true,
  imports: [IonicModule, CommonModule, FormsModule]
})
export class CreateProductPage {

  newUser: UserI;
  cargando: boolean = false;
  invalidFields: Set<string> = new Set();
  focusedFields: Set<string> = new Set();
  productosAgregados: Set<string> = new Set();

  constructor(
    private firestoreService: FirestoreService,
    private navController: NavController,
    private router: Router,
    private toastController: ToastController,
    private firestore: Firestore,
    private alertController: AlertController
  ) {
    this.initUser();
    this.checkForEdit();
    this.loadExistingProducts();
  }

  navigateToHome() {
    this.navController.navigateRoot('/home');
  }

  calculateTotal() {
    if (this.newUser.cantidad && this.newUser.valorunitario) {
      this.newUser.valortotal = this.newUser.cantidad * this.newUser.valorunitario;
    } else {
      this.newUser.valortotal = null; // Limpiar el campo si alguno de los valores es nulo
    }
  }
  

  initUser() {
    this.newUser = {
      presupuesto: null,
      unidad: null,
      producto: null,
      cantidad: null,
      valorunitario: null,
      valortotal: null,
      fechadquisicion: null,
      proveedor: null,
      id: this.firestoreService.createIdDoc(),
    };
  }

  checkForEdit() {
    const navigation = this.router.getCurrentNavigation();
    if (navigation?.extras.state) {
      const state = navigation.extras.state;
      if (state['product']) {
        this.newUser = state['product'];
      }
    }
  }

  async loadExistingProducts() {
    const productsCollection = collection(this.firestore, 'Productos');  // Asegúrate de que el nombre de la colección es correcto
    const productsSnapshot = await firstValueFrom(collectionData(productsCollection, { idField: 'id' }));
    productsSnapshot.forEach((doc: any) => {
      this.productosAgregados.add(doc.producto);
    });
  }

  async showAlert(message: string) {
    const alert = await this.alertController.create({
      message: message,
      buttons: ['Aceptar']
    });
  
    await alert.present();
  }
  

  async save() {
    await this.loadExistingProducts();  
  
    if (this.productosAgregados.has(this.newUser.producto)) {
      this.showToast('El producto ya existe.');
      return;
    }
  
    if (this.areFieldsValid()) {
      const valorTotal = this.newUser.valortotal || 0; // Si valortotal es null, asigna 0 para evitar errores
      if (valorTotal > this.newUser.presupuesto) {
        const message = 'El valor total es mayor al presupuesto.';
        await this.showAlert(message);
        return;
      }
  
      this.cargando = true;
      await this.firestoreService.createDocumentID(this.newUser, 'Productos', this.newUser.id);
      this.cargando = false;
      this.navigateToHome();
    } else {
      /*this.showToast('Por favor, complete todos los campos.');*/
      const message = 'Por favor, complete todos los campos';
      await this.showAlert(message);
    }
  }
  
  

  async openDatePicker() {
    const alert = await this.alertController.create({
      header: 'Seleccionar Fecha',
      inputs: [
        {
          name: 'date',
          type: 'date',
          value: new Date().toISOString().slice(0, 10) // Valor predeterminado: la fecha actual
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary'
        },
        {
          text: 'Aceptar',
          handler: (data) => {
            this.newUser.fechadquisicion = data.date; // Asignar la fecha seleccionada al campo
          }
        }
      ]
    });

    await alert.present();
}


  areFieldsValid(): boolean {
    this.invalidFields.clear();
    const { presupuesto, unidad, producto, cantidad, valorunitario, valortotal, fechadquisicion, proveedor } = this.newUser;

    if (!presupuesto) this.invalidFields.add('presupuesto');
    else this.invalidFields.delete('presupuesto');

    if (!unidad) this.invalidFields.add('unidad');
    else this.invalidFields.delete('unidad');

    if (!producto) this.invalidFields.add('producto');
    else this.invalidFields.delete('producto');

    if (!cantidad) this.invalidFields.add('cantidad');
    else this.invalidFields.delete('cantidad');

    if (!valorunitario) this.invalidFields.add('valorunitario');
    else this.invalidFields.delete('valorunitario');

    if (!valortotal) this.invalidFields.add('valortotal');
    else this.invalidFields.delete('valortotal');

    if (!fechadquisicion) this.invalidFields.add('fechadquisicion');
    else this.invalidFields.delete('fechadquisicion');

    if (!proveedor) this.invalidFields.add('proveedor');
    else this.invalidFields.delete('proveedor');

    return this.invalidFields.size === 0;
  }

  async showToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
      position: 'top'
    });
    toast.present();
  }

  onInputChange(field: string) {
    if (this.invalidFields.has(field)) {
      this.invalidFields.delete(field);
    }
  }
  
  isFieldInvalid(field: string): boolean {
    return this.invalidFields.has(field) && !this.focusedFields.has(field);
  }

  onFieldFocus(field: string) {
    this.focusedFields.add(field);
  }

  onFieldBlur(field: string) {
    this.focusedFields.delete(field);
  }

  isFieldFocused(field: string): boolean {
    return this.focusedFields.has(field);
  }
}
