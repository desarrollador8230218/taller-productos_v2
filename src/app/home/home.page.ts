import { Component } from '@angular/core';
import { IonHeader, IonToolbar, IonTitle, IonContent, IonLabel, IonList, IonItem, IonCard, IonInput, IonSpinner, IonButtons, IonButton, IonIcon, IonImg, IonCardHeader, IonCardTitle, IonCardContent, IonRow, IonGrid, IonCol, IonListHeader } from '@ionic/angular/standalone';
import { UserI } from '../common/models/users.models';
import { FirestoreService } from '../common/services/firestore.service';
import { FormsModule } from '@angular/forms';
import { IoniconsModule } from '../common/modules/ionicons.module';
import { NavController } from '@ionic/angular';
import { NavigationExtras } from '@angular/router';
import { AlertController } from '@ionic/angular';



@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
  standalone: true,
  imports: [IonListHeader, IonCol, IonGrid, IonRow, IonCardContent, IonCardTitle, IonCardHeader, IonImg, IonList, IonLabel, IonHeader, IonToolbar, IonTitle, IonContent, IonList, IonItem, IonInput,
    IonIcon, IonButton, IonButtons, IonSpinner, IonInput, IonCard,
    FormsModule,
    IoniconsModule
  ],
})

export class HomePage {

  users: UserI[] = [];
  cargando: boolean = false;
  filteredUsers: UserI[] = [];
  unidadFilter: string = '';
  productoFilter: string = '';
  filtro: string = '';

  constructor(private firestoreService: FirestoreService, private navController: NavController, private alertController: AlertController) {
    this.loadusers();
  }

  navigateToCreateProduct() {
    this.navController.navigateRoot('/create-product');
  }

  navigateToHome() {
    this.navController.navigateRoot('/home');
  }

  loadusers() {
    this.firestoreService.getCollectionChanges<UserI>('Productos').subscribe(data => {
      if (data) {
        this.users = data;
        this.applyFilter();
      }
    });
  }



  applyFilter() {
    this.filteredUsers = this.users.filter(user => {
      const filterValue = this.filtro.toLowerCase().trim();
      if (!filterValue) return true; // Mostrar todos los productos si el filtro está vacío
      return user.unidad.toLowerCase().includes(filterValue) || user.producto.toLowerCase().includes(filterValue);
    });
  }

  filterChanged() {
    this.applyFilter(); // Aplicar el filtro cuando el usuario escribe en el campo de entrada
  }

  async delete(user: UserI) {
    const alert = await this.alertController.create({
      header: 'Confirmar Eliminación',
      message: '¿Estás seguro de que deseas eliminar este producto?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Eliminación cancelada');
          }
        }, {
          text: 'Aceptar',
          handler: async () => {
            this.cargando = true;
            try {
              await this.firestoreService.deleteDocumentID('Productos', user.id);
              console.log('Producto eliminado');
            } catch (error) {
              console.error('Error al eliminar el producto:', error);
            } finally {
              this.cargando = false; // Asegúrate de que cargando se desactive incluso si hay un error
            }
          }
        }
      ]
    });
  
    await alert.present();
  }
  

  edit(user: UserI) {
    const navigationExtras: NavigationExtras = {
      state: {
        product: user
      }
    };
    this.navController.navigateRoot('/create-product', navigationExtras);
  }

  trackById(index: number, user: UserI): string {
    return user.id;
  }
}
